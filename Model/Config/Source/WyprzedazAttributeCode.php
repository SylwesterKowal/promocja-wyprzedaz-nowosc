<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PromocjaWyprzedazNowosc\Model\Config\Source;

class WyprzedazAttributeCode implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'yes', 'label' => __('yes')],['value' => 'no', 'label' => __('no')]];
    }

    public function toArray()
    {
        return ['yes' => __('yes'),'no' => __('no')];
    }
}

