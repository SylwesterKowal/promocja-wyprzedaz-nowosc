<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PromocjaWyprzedazNowosc\Model\Config\Source;

class SetVisibilityAs implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'catalog', 'label' => __('catalog')],['value' => 'search', 'label' => __('search')],['value' => 'catalog_search', 'label' => __('catalog_search')]];
    }

    public function toArray()
    {
        return ['catalog' => __('catalog'),'search' => __('search'),'catalog_search' => __('catalog_search')];
    }
}

