<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Kowal\PromocjaWyprzedazNowosc\Observer\Catalog;

class ProductAttributeUpdateBefore implements \Magento\Framework\Event\ObserverInterface
{
    protected $store_id = 0;

    /**
     * ProductAttributeUpdateBefore constructor.
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Kowal\PromocjaWyprzedazNowosc\Helper\Data $helperData
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement
     * @param \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Kowal\PromocjaWyprzedazNowosc\Helper\Data $helperData,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableType
    )
    {
        $this->productFactory = $productFactory;
        $this->helperData = $helperData;
        $this->collectionFactory = $collectionFactory;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->categoryLinkRepository = $categoryLinkRepository;
        $this->configurableType = $configurableType;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        try {
            $productIds = $observer->getProductIds();
            $attributes = $observer->getAttributesData();

            $this->store_id = $observer->getStoreId();

            $this->category_nowosci_id = $this->helperData->getGeneralCfg("nowosc_category_id", $this->store_id);
            $this->category_promocja_id = $this->helperData->getGeneralCfg("promocja_category_id", $this->store_id);
            $this->category_wyprzedaz_id = $this->helperData->getGeneralCfg("wyprzedaz_category_id", $this->store_id);

            $data = $observer->getData();


            if (array_key_exists('nowosci', $attributes)) {
                $collection = $this->collectionFactory->create();
                $collection->addAttributeToFilter('entity_id', ['in' => $productIds]);
                $collection->addStoreFilter($this->store_id);
                $collection->addAttributeToSelect('*');
                foreach ($collection as $product) {
                    if ($attributes['nowosci'] != $product->getNowosci()) {
                        $categories = $this->setCategory($product, $attributes['nowosci'], $this->category_nowosci_id);
                        $product->setCategoryIds($categories);
                        $product->save();
                    }
                }
            }

            if (array_key_exists('wyprzedaz', $attributes)) {
                $collection = $this->collectionFactory->create();
                $collection->addAttributeToFilter('entity_id', ['in' => $productIds]);
                $collection->addStoreFilter($this->store_id);
                $collection->addAttributeToSelect('*');
                foreach ($collection as $product) {
                    if ($attributes['wyprzedaz'] != $product->getWyprzedaz()) {
                        $categories = $this->setCategory($product, $attributes['wyprzedaz'], $this->category_wyprzedaz_id);
                        $product->setCategoryIds($categories);
                        $vivibility = $product->getVisibility();
                        $parentIds = $this->configurableType->getParentIdsByChild($product->getId());

                        if ($attributes['wyprzedaz'] == 1 && $vivibility == 1) {
                            $product->setVisibility(4);
//                            $images = $product->getMediaGalleryImages();
//
//                            if (count($images) == 0) {
//
//                                if (isset($parentIds[0])) {
//                                    $parentProduct = $this->productFactory->load($parentIds[0]);
//                                    $images = $parentProduct->getMediaGalleryImages();
//                                    if (count($images) > 0) {
//                                        foreach ($images as $image) {
//
//                                           file_put_contents ("/home/m2tande/public_html/var/tmp/__images_mass.txt", "IMAGES", FILE_APPEND);
//
//
//                                            break;
//                                        }
//                                    }
//                                }
//                            }else{
//                                file_put_contents("/home/m2tande/public_html/var/tmp/__images_mass.txt", "HAS IMAGES", FILE_APPEND);
//                            }
                        } else if (!empty($parentIds)) {
                            $product->setVisibility(1);
                        }

                        $product->save();
                    }
                }
            }

            if (array_key_exists('promocja', $attributes)) {
                $collection = $this->collectionFactory->create();
                $collection->addAttributeToFilter('entity_id', ['in' => $productIds]);
                $collection->addStoreFilter($this->store_id);
                $collection->addAttributeToSelect('*');
                foreach ($collection as $product) {
                    if ($attributes['promocja'] != $product->getPromocja()) {
                        $categories = $this->setCategory($product, $attributes['promocja'], $this->category_promocja_id);
                        $product->setCategoryIds($categories);
                        $product->save();

                    }
                }
            }
        } catch (\Execption $e) {
            file_put_contents("/home/m2tande/public_html/var/tmp/error_mass.txt", $e->getMessage(), FILE_APPEND);
        }
    }

    public function setCategory($product, $new_value, $category_id)
    {
        $categories = ($product->getCategoryIds()) ? $product->getCategoryIds() : [];
        if ($new_value == 1) { // dodajemy kategorie
            if (!in_array($category_id, $categories)) {
                $categories[] = $category_id;
            }

        } else { // usuwamy kategorię
            if (in_array($category_id, $categories)) {
                if (($key = array_search($category_id, $categories)) !== false) {
                    unset($categories[$key]);
                }
            }
        }

        return $categories;
    }

}

