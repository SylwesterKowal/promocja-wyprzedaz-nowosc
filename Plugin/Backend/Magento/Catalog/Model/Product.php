<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PromocjaWyprzedazNowosc\Plugin\Backend\Magento\Catalog\Model;

class Product
{
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Kowal\PromocjaWyprzedazNowosc\Helper\Data $helperData,
        \Magento\Catalog\Api\CategoryLinkManagementInterface $categoryLinkManagement,
        \Magento\Catalog\Model\CategoryLinkRepository $categoryLinkRepository
    )
    {
        $this->productFactory = $productFactory;
        $this->helperData = $helperData;
        $this->categoryLinkManagement = $categoryLinkManagement;
        $this->categoryLinkRepository = $categoryLinkRepository;

    }


    public function beforeSave(
        \Magento\Catalog\Model\Product $subject
    )
    {
        $curent_produt_id = $subject->getId();
        $product = $this->productFactory->create()
            ->setStoreId($subject->getStoreId())
            ->load($curent_produt_id);

        if ($subject->getStoreId() != 0) {
            $this->category_nowosci_id = $this->helperData->getGeneralCfg("nowosc_category_id", $subject->getStoreId());
            $this->category_promocja_id = $this->helperData->getGeneralCfg("promocja_category_id", $subject->getStoreId());
            $this->category_wyprzedaz_id = $this->helperData->getGeneralCfg("wyprzedaz_category_id", $subject->getStoreId());


            if($this->category_nowosci_id ) $subject = $this->setCategory($subject, $product, $this->category_nowosci_id, 'getNowosci');
            if($this->category_wyprzedaz_id ) $subject = $this->setCategory($subject, $product, $this->category_wyprzedaz_id, 'getWyprzedaz');
            if($this->category_promocja_id) $subject = $this->setCategory($subject, $product, $this->category_promocja_id, 'getPromocja');
        } else {
            $websiteIds = $product->getWebsiteIds();
            foreach ($websiteIds as $websiteId) {
                $this->category_nowosci_id = $this->helperData->getWebsiteCfg("nowosc_category_id", $websiteId);
                $this->category_promocja_id = $this->helperData->getWebsiteCfg("promocja_category_id", $websiteId);
                $this->category_wyprzedaz_id = $this->helperData->getWebsiteCfg("wyprzedaz_category_id", $websiteId);


                if($this->category_nowosci_id ) $subject = $this->setCategory($subject, $product, $this->category_nowosci_id, 'getNowosci');
                if($this->category_wyprzedaz_id) $subject = $this->setCategory($subject, $product, $this->category_wyprzedaz_id, 'getWyprzedaz');
                if($this->category_promocja_id) $subject = $this->setCategory($subject, $product, $this->category_promocja_id, 'getPromocja');
            }

        }
        return [];
    }

    public function setCategory($subject, $product, $category_id, $attr)
    {
        if ($subject->$attr() != $product->$attr()) {
            $categories = ($subject->getCategoryIds()) ? $subject->getCategoryIds() : [];
            if ($subject->$attr() == 1) { // dodajemy kategorie
                if (!in_array($category_id, $categories)) {
                    $categories[] = $category_id;
                    $subject->setCategoryIds($categories);
                }

            } else { // usuwamy kategorię
                if (in_array($category_id, $categories)) {
                    if (($key = array_search($category_id, $categories)) !== false) {
                        unset($categories[$key]);
                    }
                    $subject->setCategoryIds($categories);
                }
            }
        }
        return $subject;
    }
}

