<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\PromocjaWyprzedazNowosc\Plugin\Magento\Catalog\Block\Product;

class ListProduct
{

    public function __construct(
        \Kowal\PromocjaWyprzedazNowosc\Helper\Data $dataHelper
    ) {
        $this->dataHelper = $dataHelper;
    }

    public function afterGetProductPrice(
        \Magento\Catalog\Block\Product\ListProduct $subject,
        $result,
        \Magento\Catalog\Model\Product $product
    )
    {
        $store_id = $product->getStoreId() ?? 0;
        $kolor_nowosc =  $this->dataHelper->getWebsiteCfg("kolor_nowosc", $store_id) ?? "#aaa";
        $kolor_promocja =   $this->dataHelper->getWebsiteCfg("kolor_promocja", $store_id) ?? "#aaa";
        $kolor_wyprzedaz =  $this->dataHelper->getWebsiteCfg("kolor_wyprzedaz", $store_id) ?? "#aaa";
        $kolor_dostawa =  $this->dataHelper->getWebsiteCfg("kolor_dostawa", $store_id) ?? "#aaa";

        $label_nowosc =  $this->dataHelper->getWebsiteCfg("label_nowosc", $store_id) ?? __("New product");
        $label_promocja =   $this->dataHelper->getWebsiteCfg("label_promocja", $store_id) ?? __("Promotion");
        $label_wyprzedaz =  $this->dataHelper->getWebsiteCfg("label_wyprzedaz", $store_id) ?? __("Outlet");
        $label_dostawa =  $this->dataHelper->getWebsiteCfg("label_dostawa", $store_id) ?? __("Free delivery");

        $darmowa_dostawa = $product->getDarmowaDostawa();
        $nowosci = $product->getNowosci();
        $promocja = $product->getPromocja();
        $wyprzedaz = $product->getWyprzedaz();


        $result .= "<div class=\"kowal-promocja-wyprzedaz-nowosc-list\">";
        if ($darmowa_dostawa) {
            $result .= "<div class=\"kowal-label kowal-darmowa-dostawa\" style=\"background-color: " . $kolor_dostawa . "\">" . $label_dostawa . "</div>";
        }
        if ($nowosci) {
            $result .= "<div class=\"kowal-label kowal-nowosc\" style=\"background-color: " . $kolor_nowosc . "\">" . $label_nowosc . "</div>";
        }
        if ($promocja && !$wyprzedaz || $promocja && $wyprzedaz) {
            $result .= "<div class=\"kowal-label kowal-promocja\"  style=\"background-color: " . $kolor_promocja . "\">" . $label_promocja . "</div>";
        }
        if ($wyprzedaz && !$promocja) {
            $result .= "<div class=\"kowal-label kowal-wyprzedaz\"  style=\"background-color: " . $kolor_wyprzedaz . "\">" . $label_wyprzedaz . "</div>";
        }
        $result .= "</div>";

        return $result;
    }
}
