# Mage2 Module Kowal PromocjaWyprzedazNowosc

    ``kowal/module-promocjawyprzedaznowosc``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Ustaw produkt jako P W N masowo

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_PromocjaWyprzedazNowosc`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-promocjawyprzedaznowosc`
 - enable the module by running `php bin/magento module:enable Kowal_PromocjaWyprzedazNowosc`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable (promocja_wyprzedaz_nowosc/settings/enable)

 - promocja_category_id (promocja_wyprzedaz_nowosc/settings/promocja_category_id)

 - wyprzedaz_category_id (promocja_wyprzedaz_nowosc/settings/wyprzedaz_category_id)

 - nowosc_category_id (promocja_wyprzedaz_nowosc/settings/nowosc_category_id)

 - promocja_attribute_code (promocja_wyprzedaz_nowosc/settings/promocja_attribute_code)

 - promocja_attribute_value (promocja_wyprzedaz_nowosc/settings/promocja_attribute_value)

 - wyprzedaz_attribute_code (promocja_wyprzedaz_nowosc/settings/wyprzedaz_attribute_code)

 - wyprzedaz_attribute_value (promocja_wyprzedaz_nowosc/settings/wyprzedaz_attribute_value)

 - nowosc_attribute_code (promocja_wyprzedaz_nowosc/settings/nowosc_attribute_code)

 - copy_image_form_parent_if_empty (promocja_wyprzedaz_nowosc/settings/copy_image_form_parent_if_empty)

 - copy_description_form_parent_if_empty (promocja_wyprzedaz_nowosc/settings/copy_description_form_parent_if_empty)

 - set_visibility_as (promocja_wyprzedaz_nowosc/settings/set_visibility_as)


## Specifications

 - Plugin
	- beforeSave - Magento\Catalog\Model\Product > Kowal\PromocjaWyprzedazNowosc\Plugin\Backend\Magento\Catalog\Model\Product

 - Observer
	- catalog_product_attribute_update_before > Kowal\PromocjaWyprzedazNowosc\Observer\Backend\Catalog\ProductAttributeUpdateBefore

 - Console Command
	- Promocja

 - Console Command
	- Wyprzedaz

 - Console Command
	- Nowosc


## Attributes

 - Product - nowosc (nowosc)

 - Product - promocja_wyprzedaz (promocja_wyprzedaz)

